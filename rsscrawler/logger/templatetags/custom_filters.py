'''
Contains the custom django-template filters for getting the timestamp or the nth value of a list
'''

from django import template
register = template.Library()


@register.filter(name="list_get")
def list_get(li, val):
    '''
    Django-template filter for getting the val-th value of a list
    :param li: The list containing the wanted item
    :param val: The index of the wanted item
    :return: The wanted item
    '''
    return li[val]


@register.filter(name="timestamp")
def ts(obj):
    '''
    Django-template filter for getting the timestamp attribute of a object
    :param obj: The object
    :return: The timestamp attribute value of the wanted object
    '''
    return obj.timestamp
