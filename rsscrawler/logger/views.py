'''
Django views file for projecting methods to django template htmls
'''
from django.template import loader
# Create your views here.
import datetime
from crawler.models import Log
from crawler.models import Error
from django.http import HttpResponse


def logging_web(request):
    '''
    Displays the last 100 log entries on /log/latest
    :return: The HTTP response with the context containing the information from the database that is displayed on the
    website
    '''
    log_length = Log.objects.count()
    if len(request.GET) == 0 or request.GET['start'] == '' or request.GET['end'] == '':
        start = max(log_length - 100, 0)
        end = log_length
    else:
        start = max(int(request.GET['start']) - 1, 0)
        end = int(request.GET['end'])

    latest_logs_list = Log.objects.order_by('timestamp')[start:end]
    err_dict = {}
    for log in latest_logs_list:
        err_dict[log] = []
        for corresponding_error in Error.objects.filter(timestamp=log):
            err_dict[log].append(corresponding_error)

    template = loader.get_template('logfiles.html')
    context = {'latest_logs_list': latest_logs_list,
               'err_dict': err_dict,
               'log_length': log_length,
               }
    return HttpResponse(template.render(context, request))


def logs_by_date(request):
    '''
    Displays the requested interval of logs
    Interval is given by HTTP parameters 'start' and 'end'
    :param request: Contains the needed parameters for the interval
    :return: The HTTP response with the context containing the information from the database that is displayed on the
    website
    '''
    if len(request.GET) == 0 or request.GET['start'] == '' or request.GET['end'] == '':
        return logging_web(request)

    log_length = Log.objects.count()
    # exception abfangen!
    # datetime erstellung optimieren
    last = request.GET['start'].split(',')
    last_dt = datetime.datetime(int(last[0]), int(last[1]), int(last[2]), int(last[3]), int(last[4]))
    first = request.GET['end'].split(',')
    first_dt = datetime.datetime(int(first[0]), int(first[1]), int(first[2]), int(first[3]), int(first[4]))

    latest_logs_list = Log.objects.filter(timestamp__range=(first_dt, last_dt))

    err_dict = {}
    for log in latest_logs_list:
        err_dict[log] = []
        for error in Error.objects.all():
            if error.timestamp == log:
                err_dict[log].append(error)

    template = loader.get_template('by_date.html')
    context = {'latest_logs_list': latest_logs_list,
               'err_dict': err_dict,
               'log_length': log_length,
               }
    return HttpResponse(template.render(context, request))


def last_0(request):
    '''
    Display all the logs containing less than or equal 15 entries that already were stored in database
    These logs are most likely to have been preceeded by errors
    Interval is given by HTTP parameters ''
    :param request: Contains the needed parameters for the interval
    :return: The HTTP response with the context containing the information from the database that is displayed on the
    website
    '''
    log_length = Log.objects.count()
    latest_logs_list = Log.objects.filter(duplicate_entries__lte=15)
    err_dict = {}
    for log in latest_logs_list:
        err_dict[log] = []
        for error in Error.objects.all():
            if error.timestamp == log:
                err_dict[log].append(error)

    template = loader.get_template('logfiles.html')
    context = {'latest_logs_list': latest_logs_list,
               'err_dict': err_dict,
               'log_length': log_length,
               }
    return HttpResponse(template.render(context, request))
