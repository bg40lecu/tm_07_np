from django.db import models


class Article(models.Model):
    id = models.CharField(max_length=128)
    id.primary_key = True
    publisher = models.CharField(max_length=10)
    entry = models.TextField()
    html = models.TextField()


class Log(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    timestamp.primary_key = True
    duplicate_entries = models.IntegerField()
    new_entries = models.IntegerField()


class Error(models.Model):
    timestamp = models.ForeignKey(Log, on_delete=models.CASCADE)
    err_type = models.CharField(max_length=255)
    caused_by = models.CharField(max_length=192)


class Scrape(models.Model):
    id = models.OneToOneField(Article, on_delete=models.CASCADE, primary_key=True)
    article = models.TextField()


class Topic(models.Model):
    id = models.OneToOneField(Article, on_delete=models.CASCADE, primary_key=True)
    topic = models.CharField(max_length=8)


class ArticleTime(models.Model):
    id = models.OneToOneField(Article, on_delete=models.CASCADE, primary_key=True)
    timestamp = models.DateTimeField()
    dow = models.IntegerField()
    hour = models.IntegerField()
    art_length = models.IntegerField()
