import bs4
from crawler.models import Article, Scrape
import traceback


def welt_scrape(new_entry_id):
    '''
    Gets WELT article to given new_entry_id from database, scrapes the pure article text and saves it to
    the crawler_scrape table
    :param new_entry_id: crawler_article id of WELT article that shall be scraped
    '''
    try:
        source_obj = Article.objects.get(id=new_entry_id)
        source = source_obj.html
        text = ''
        soup = bs4.BeautifulSoup(source, 'lxml')

        for i in soup.find_all(['p', 'h3', 'h2', 'div']):
            if i.name == 'p' and i.get('class') is None:
                text += i.text + '\n'
            elif i.name in ('h2', 'h3'):
                text += i.text + '\n'
            elif not i.get('class') is None:
                if 'c-summary' in i.get('class')[0]:
                    text += i.text + '\n'

        Scrape(id=source_obj, article=text).save()
    except Exception as e:
        traceback.print_exc()