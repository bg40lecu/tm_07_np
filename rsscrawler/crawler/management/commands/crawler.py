"""
When used as a django-command, it crawls certain rssfeeds from certain publishers
and stores the raw rssfeeds and the raw html for each yet unstored article in
the database for further analysis.
"""

from django.core.management.base import BaseCommand
from crawler.models import Article
from crawler.models import Log
from crawler.models import Error
from urllib.request import urlopen
import feedparser
import hashlib
import json


class Command(BaseCommand):
    """
    Django-command to crawl certain rssfeeds from certain publishers
    and stores the raw rssfeeds and the raw html for each yet unstored article in
    the database for further analysis.
    """
    
    def _hash(self, string):
        """
        Hashes a given string with sha512 and returns it in its hex-representation.

        :param string: string to be hashed
        :returns: hex-representation of the sha512 applied to string
        """
        hasher = hashlib.sha512()
        hasher.update(string.encode())
        return hasher.hexdigest()
    
    def add_arguments(self, parser):
        """
        Adds the -l option to activate verbose output of the crawl process.
        """
        parser.add_argument("-l", action="store_true") 

    def handle(self, *args, **options):
        """
        Crawls the rssfeeds specified by the **urls**-variable.
        Logs of the crawl process are generated to analyse the crawl process
        and identify problems.
        Articles are only crawled if they are not already in the database and
        if they are not premium.
        """
        
        verbose = False
        if options["l"]:
             verbose = True

        urls = {
                "Tagesschau": "http://www.tagesschau.de/xml/rss2",
                "FAZ": "http://www.faz.net/rss/aktuell",
                "SZ": "http://rss.sueddeutsche.de/app/service/rss/alles/index.rss?output=rss",
                "BILD": "https://www.bild.de/rssfeeds/rss3-20745882,feed=alles.bild.html",
                "Spiegel": "http://www.spiegel.de/schlagzeilen/index.rss",
                "Welt": "https://www.welt.de/feeds/latest.rss"
        }
        duplicates = 0
        new_entries = 0

        log = Log(duplicate_entries=0, new_entries=0)
        log.save()
        id_url = None

        for publisher, url in urls.items():
            feed = feedparser.parse(url)
            entries = feed['entries']
            
            if verbose:
                print()
                print("{} Anzahl: {}".format(publisher, len(entries)))
                count = 0

            for entry in entries:
                if verbose:
                    count = count + 1
                    print("\r{}".format(count), end="")

                try:

                    ### TEST OB BILD ODER SPIEGEL PREMIUM ARTIKEL -- FALLS JA NICHT CRAWLEN ###
                    premium = False

                    if publisher == 'BILD':
                        if 'BILDplus Inhalt' in entry['title']:
                            premium = True
                    elif publisher == 'Spiegel':
                        if entry['tags'][0]['term'] == 'SPIEGEL+':
                            premium = True
                    elif publisher == "Welt":
                        if entry['welt_premium'] == 'true':
                            premium = True

                    if not premium:
                        id_url = entry['id']
                        id_hash = self._hash(str(id_url))

                        # check if article is in db.
                        try:
                            Article.objects.get(id=id_hash)
                        except Exception:
                            new_entries += 1
                            article_url = entry['link']
                            with urlopen(article_url) as site:
                                text = site.read().decode("utf-8")

                            article = Article(id=id_hash, publisher=publisher, entry=json.dumps(entry, ensure_ascii=False), html=text)
                            article.save()
                        else:
                            duplicates += 1

                except Exception as e:
                    Error(timestamp=log, err_type='{}\n{}'.format('Error',  str(e)[:245]), caused_by=id_url).save()
                    print(e)
                    if article_url:
                        print(article_url)

            # log to logging page in django gui
            # print(duplicate_per_publisher_counter + "duplicates")

        log.duplicate_entries = duplicates
        log.new_entries = new_entries
        log.save()  # final update log
