'''
When used as a django-command scrapes all the not yet scraped articles from crawler_article
'''

from django.core.management.base import BaseCommand
from crawler.management.commands import bild_scraper, welt_scraper
from crawler.models import Article, Scrape
from newspaper import Article as Art
import traceback
from analysis.analysis_methods import update_words_by


class Command(BaseCommand):
    '''
    Scrapes all the not yet scraped articles from crawler_article using bild_scraper, welt_scraper or a predefined
    scraper suiting most websites. It also looks for paywalls in htmls to not save premium articles
    '''
    def handle(self, *args, **options):
        try:
            faz_premium = False
            faz_premium_indicator = '<div class="atc-ContainerPaywall_ContentBlocker"></div>'


            already_there = Scrape.objects.values('id')
            new_entry_ids = Article.objects.values('id').exclude(id__in=already_there)
            #                               FAZ PLUS
            exception_strings = ['js-ctn-PaywallInfo ctn-PaywallInfo', ]

            for id_dict in new_entry_ids:
                for new_entry_id in id_dict.values():
                    pub = Article.objects.get(id=new_entry_id).publisher
                    html = Article.objects.get(id=new_entry_id).html
                    if any(x in Article.objects.get(id=new_entry_id).html for x in exception_strings):
                        pass
                    else:
                        art = Art('')
                        source_obj = Article.objects.get(id=new_entry_id)

                        if faz_premium_indicator in source_obj.html:
                            Article.objects.filter(id=new_entry_id).delete()
                        else:
                            art.set_html(source_obj.html)
                            art.parse()
                            text = art.title + '\n' + art.text

                            Scrape(id=source_obj, article=text).save()

            update_words_by(new_entry_ids)
        except Exception as e:
            traceback.print_exc()
