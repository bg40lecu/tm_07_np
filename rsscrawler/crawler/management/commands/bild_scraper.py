import bs4
from urllib.error import URLError
from crawler.models import Article, Scrape
import traceback


def bild_scrape(new_entry_id):
    '''
    Gets BILD article to given new_entry_id from database, scrapes the pure article text and saves it to
    the crawler_scrape table
    :param new_entry_id: crawler_article id of BILD article that shall be scraped
    '''
    try:

        source_obj = Article.objects.get(id=new_entry_id)
        source = source_obj.html
        soup = bs4.BeautifulSoup(source, 'lxml')
        text = ''
        titles_and_p_tags = soup.find_all(['p', 'h1', 'h2', 'h3'])

        for i in titles_and_p_tags:

            if i.name == 'p' and i.get('class') is None:

                if 'wap2.bild.de' not in str(i.text):

                    text += i.text + '\n'
            elif i.get('class') == ['crossheading']:

                text += i.text + '\n'

            elif i.get('id') == 'cover':

                children = i.findChildren()

                for child in children:

                    if not (child.get('class') == ['hidden']):

                        text += child.text + '\n'

            elif i.get('class') == ['subhead']:

                text += i.text + '\n'

        Scrape(id=source_obj, article=text).save()

    except Exception as e:
        traceback.print_exc()
