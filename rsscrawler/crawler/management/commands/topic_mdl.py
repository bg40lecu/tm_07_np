from django.core.management.base import BaseCommand
from gensim import *
from crawler.models import Scrape

import spacy
from spacy.lang.de import German
parser = German()

from spacy.lemmatizer import Lemmatizer
    
from nltk.corpus import stopwords

import string

import gensim
from gensim import corpora

# REQUIRES LIBRARIES: GENSIM , NLTK, SPACY:
# pip install -U gensim
# pip install -U nltk
# pip install -U spacy (maybe sudo apt-get install build-essential python-dev git)
# python3 -m spacy download de


class Command(BaseCommand):

    def prepare_corpus(self, texts):
        
        # Create Dictionary
        id2word = corpora.Dictionary(texts)

        # Term Document Frequency
        corpus = [id2word.doc2bow(text) for text in texts]

        # View
        print(corpus)

        #print([[(id2word[id], freq) for id, freq in cp] for cp in corpus[:1]])

        lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                           id2word=id2word,
                                           num_topics=10, 
                                           random_state=100,
                                           update_every=1,
                                           chunksize=100,
                                           passes=50,
                                           alpha='auto',
                                           per_word_topics=True)

        print(lda_model.print_topics())
        doc_lda = lda_model[corpus]



    #pulls queryset objects from db and returns an array of articles (strings)
    def get_data(self):
        article_dict_list = list(Scrape.objects.values('article'))
        print('1')
        article_arr = []
        for article_dict in article_dict_list:
            article_arr.append(article_dict.get("article", "none"))
        return article_arr

    #takes an array of articles(strings) and returns an array of spacy docs objects
    #print(article_docs[index], article_docs[index][1].lemma_)
    def data_to_docs(self, article_arr):
        nlp = spacy.load('de')
        # build an array of docs
        article_docs = []
        for index, article in enumerate(article_arr):
            article_docs.append(nlp(article))
        return article_docs


    """ def lemmatize(self, docs):
        lemmatizer = Lemmatizer()
        lemmatized = []
        for article in docs:
            for word in article:
                lemmatized.append(word.lemma_)
        return lemmatized """
    
    #receives docs, returns lemmatized list of list of str
    def lemmatize(self, texts, allowed_postags=['NOUN', 'ADJ', 'VERB', 'ADV']):
        texts_out = []
        for article in texts:
            texts_out.append([token.lemma_ for token in article if token.pos_ in allowed_postags])
        return texts_out
    
    #helper function
    def clean_func(self, doc):
        exclude = set(string.punctuation) 
        stop_words = set(stopwords.words('german'))
        stop_free = []

        #remove stopwords
        clean = [[word for word in article if word not in [*exclude, *stop_words]] for article in doc]
        #punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
        #print(article_docs[index], article_docs[index][1].lemma_)
        return clean

    #takes a list of doc objects and returns a list of list of strings
    def clean(self, docs):
        #remove_stopwords_punctuation_lower_
        #case
        cleaned_list = self.clean_func(docs)
        #
        # print(cleaned_list)
        return cleaned_list

    #main func
    def handle(self, *args, **options):
        data_array = self.get_data()
        docs = self.data_to_docs(data_array)
        lemmatized = self.lemmatize(docs)
        cleaned_list = self.clean(lemmatized)  #returns matrix (list of list of str)
        corpus = self.prepare_corpus(cleaned_list)
