'''
When used as a django-command deletes all premium articles from publishers BILD, WELT, SPIEGEL, FAZ
by looking for certain paywalls on html or hints in the rssfeed-entries
'''

from django.core.management.base import BaseCommand
from crawler.models import Article, Scrape


class Command(BaseCommand):
    '''
    Deletes all premium articles from publishers BILD, WELT, SPIEGEL, FAZ
    by looking for certain paywalls on html or hints in the rssfeed-entries
    '''

    def handle(self, *args, **options):
        '''
        Deletes all premium articles from publishers BILD, WELT, SPIEGEL, FAZ
        by looking for certain paywalls on html or hints in the rssfeed-entries
        '''
        bild_filter = Article.objects.filter(entry__contains='BILDplus Inhalt')
        welt_filter = Article.objects.filter(entry__contains='welt_premium": "true"')
        spiegel_filter = Article.objects.filter(entry__contains='SPIEGEL+')
        faz_filter = Scrape.objects.filter(article__contains='<div class="atc-ContainerPaywall_ContentBlocker"></div>')

        for news_filter in [bild_filter, welt_filter, spiegel_filter, faz_filter]:
            for obj in news_filter:
                obj.delete()
