'''
When used as a django-command inserts information about articletimestamps and articlelengths to crawler_articletime
used for creating the heatmaps containing information about publishes and average lengths of article published at
certain times of the week
'''

from django.core.management.base import BaseCommand
from crawler.models import ArticleTime, Article, Scrape
import json
from datetime import datetime, timezone

class Command(BaseCommand):
    '''
    Inserts information about articletimestamps and articlelengths to crawler_articletime
    used for creating the heatmaps containing information about publishes and average lengths of article published at
    certain times of the week
    '''

    def insert_length(self):
        '''
        Inserts the lengths to crawler_articletime
        Applied with -length
        '''
        print(Scrape.objects.count())
        j = 0
        for i in range(1, 11):
            lengths = list(Scrape.objects.all()[(i - 1) * Scrape.objects.count() / 10:i * Scrape.objects.count() / 10])
            for art in lengths:
                art_length = len(art.article.split(' '))

                art_time_obj = ArticleTime.objects.get(id=art.id)
                art_time_obj.art_length = art_length
                art_time_obj.save()
                j += 1
                print('Artikel #{} Länge {} zugefügt'.format(j, art_length))

    def insert_timestamp(self):
        '''
        Inserts the timestamps and the day of week and the hour of the day on which an article was published
        '''
        q = list(Article.objects.values('id', 'entry'))
        for art in q:
            entry = json.loads(art['entry'])
            ts_list = entry['published_parsed'] # [year, month, day, hour, minute, second, dow, doy, tz]
            ts = datetime(ts_list[0], ts_list[1], ts_list[2], ts_list[3], tzinfo=timezone.utc)

            ArticleTime(Article.objects.get(id=art['id']), ts, ts.weekday(), ts.hour, 0).save()

    def add_arguments(self, parser):
        '''
        Adds the -length option to add articlelengths to the table
        '''
        parser.add_argument("-length", action="store_true",
                            help='adds length of articles to database (else 0)')

    def handle(self, *args, **options):
        self.insert_timestamp()
        if options['length']:
            self.insert_length()
