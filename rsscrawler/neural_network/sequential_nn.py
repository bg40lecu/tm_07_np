import pandas as pd
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.models import Sequential
from keras.layers import Activation, Dense, Dropout
from keras import regularizers
from sklearn.preprocessing import LabelBinarizer
import sklearn.datasets as skds
from pathlib import Path
import matplotlib.pyplot as plt
import itertools
from sklearn.metrics import confusion_matrix
import datetime
import pickle

# This is the model we used to classify all articles in our database

def start_sequential(stops):

    # uses a folder based on your stopword preferences

    if (stops):

        path = "analysis/data_with_stops"
    else:
        path = "analysis/data_without_stops"

    files = skds.load_files(path, load_content = False)

    label_index = files.target
    label_names = files.target_names
    labelled_files = files.filenames

    data_tags = ["filename", "category", "news"]
    data_list = []

    i = 0

    # Getting the files from path

    for file in labelled_files:

        data_list.append((file, label_names[label_index[i]], Path(file).read_text()))
        i += 1

    data = pd.DataFrame.from_records(data_list, columns = data_tags)

    # These were the params that worked best for us

    num_labels = 11
    vocab_size = 100000
    batch_size = 100
    num_epochs = 12

    # splitting the data into train and test data

    train_size = int(len(data) * .8)

    train_posts = data['news'][:train_size]
    train_tags = data['category'][:train_size]

    test_posts = data['news'][train_size:]
    test_tags = data['category'][train_size:]

    # build the vocabulary

    tokenizer = Tokenizer(num_words=vocab_size)
    tokenizer.fit_on_texts(train_posts)

    # builds a numeric vector

    x_train = tokenizer.texts_to_matrix(train_posts, mode='tfidf')
    x_test = tokenizer.texts_to_matrix(test_posts, mode='tfidf')

    # labels to one hot vectors

    encoder = LabelBinarizer()
    encoder.fit(train_tags)
    y_train = encoder.transform(train_tags)
    y_test = encoder.transform(test_tags)

    model = Sequential()
    model.add(Dense(512, input_shape=(vocab_size,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.3))
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.3))
    model.add(Dense(num_labels))
    model.add(Activation('softmax'))
    model.summary()

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    history = model.fit(x_train,
              y_train,
              batch_size=batch_size,
              epochs=num_epochs,
              verbose=1,
              validation_split=0.1)

    ##########################################

    # Plot training & validation accuracy values
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

    ##########################################

    score = model.evaluate(x_test,
                           y_test,
                           batch_size = batch_size,
                           verbose = 1)

    print('Test accuracy:', score[1])

    # This is necessary to construct a confusion matrix which shows the percentages of the network classifying
    # an article in the wrong class

    fig = plt.figure()
    fig.set_size_inches(14, 12, forward=True)
    fig.align_labels()

    normalize = True
    title = 'Prognose des Netzes'
    cmap = plt.cm.Reds
    y_pred = model.predict(x_test)
    cm = confusion_matrix(np.argmax(y_test, axis = 1), np.argmax(y_pred, axis = 1))
    classes = np.asanyarray(label_names)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    if (normalize):

        cm = cm.astype('float') / cm.sum(axis = 1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation = 'nearest', cmap = cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation = 90)
    plt.yticks(tick_marks, classes)

    fmt = ' .2f' if normalize else 'd'
    threshold = cm.max() / 2

    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > threshold else "black")

    date = datetime.datetime.now()

    fig.savefig('txt_classification-seq-' + str(date) + '.png', pad_inches=5.0)

    plt.show()

    model.model.save('seq.h5')

    with open('tokenizer.pickle', 'wb') as handle:
        pickle.dump(tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)