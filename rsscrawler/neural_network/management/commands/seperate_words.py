from django.core.management.base import BaseCommand
from crawler.models import Scrape


class Command(BaseCommand):

    # this command fixes a bug that was implemented in the crawler, which concatenates the titles and beginnings of the texts
    # can be run to improve results slightly by removing words that were created by joining the last word of the title with the first word of the text

    def handle(self, *args, **options):

        texts = Scrape.objects.filter(id__publisher='Spiegel')
        for text in texts:
            found = False
            for i in range(0, len(text.article) - 1):
                if text.article[i].islower() and text.article[i + 1].isupper() and \
                  not ('{}{}{}'.format(text.article[i - 1], text.article[i], text.article[i + 1]) == 'AfD'):
                    found = True
                    text.article = text.article[:i + 1] + ' ' + text.article[i + 1:]
                    text.save()
                if found:
                    break