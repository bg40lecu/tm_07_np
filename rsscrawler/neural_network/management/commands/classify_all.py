from django.core.management.base import BaseCommand
from crawler.models import Topic, Article, Scrape
from keras.models import load_model
from keras.preprocessing.text import Tokenizer
import pickle
import pandas as pd
import numpy as np
import tqdm

# basically uses a pre-trained neural network and tokenizer to classify all data contained in the database

class Command(BaseCommand):

    # preprocesses the data that is about to be classified in the same way the train-data was preprocessed to train the network

    def remove_stopwords(self, text):

        with open("analysis/german_stopwords_plain_modified.txt", "r") as file:
            stopwords = file.read().splitlines()

            text_without_stops = ''

            words = text.split()
            for word in words:

                # cleans the words

                word = word.replace("\"", "")
                word = word.replace(",", "")
                word = word.replace(".", "")
                word = word.replace("?", "")
                word = word.replace("!", "")
                word = word.replace("(", "")
                word = word.replace(")", "")
                word = word.replace(":", "")
                word = word.replace("]", "")
                word = word.replace("[", "")

                is_stopword = False
                for stopword in stopwords:

                    if word == stopword or word == '' or word == '-':
                        is_stopword = True
                        break
                if not is_stopword:
                    text_without_stops += word + ' '

            return text_without_stops

    def handle(self, *args, **options):

        model = load_model('neural_network/nn_related_stuff/seq.h5')
        tokenizer = Tokenizer()
        labels = ['GES', 'KUL', 'LUL', 'MOB', 'NET', 'PAN', 'POL', 'REI', 'SPO', 'WIR', 'WIS']

        with open('neural_network/nn_related_stuff/tokenizer.pickle', 'rb') as handle:
            tokenizer = pickle.load(handle)

        # 100 articles at a time will be sent trough the network
        # 100 at a time seem to be just the right amount for the memory
        # basically grabs 100 articles from the database and classifies them

        chunk_size = 100
        big_chunks = int(Scrape.objects.count()/chunk_size)
        remainder = Scrape.objects.count() % chunk_size

        for i in tqdm.tqdm(range(1, big_chunks + 2)):
            if i == big_chunks + 1:
                scrape_objects = Scrape.objects.all()[i * chunk_size:]
            else:
                scrape_objects = Scrape.objects.all()[(i-1) * chunk_size:i * chunk_size]
            ids = []
            texts = []
            if i == big_chunks + 1:
                number_of_items_in_ids = remainder
            else:
                number_of_items_in_ids = chunk_size
            for j in range(0, number_of_items_in_ids):
                ids.append(scrape_objects[j].id)
                texts.append(self.remove_stopwords(scrape_objects[j].article))
            x_data_series = pd.Series(texts)
            x_tokenized = tokenizer.texts_to_matrix(x_data_series, mode='tfidf')
            j = 0
            for x_t in x_tokenized:
                prediction = model.predict(np.array([x_t]))
                predicted_label = labels[np.argmax(prediction[0])]
                #print("id -> ", ids[j].id[:30], "||  Predicted label: " + predicted_label)
                Topic(id=ids[j], topic=predicted_label).save()
                j += 1

