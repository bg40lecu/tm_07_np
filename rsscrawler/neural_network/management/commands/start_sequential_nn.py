from django.core.management.base import BaseCommand
from neural_network.sequential_nn import start_sequential
from analysis.analysis_methods import generate_nn_data
from crawler.models import Article

class Command(BaseCommand):

    topics = ["Kultur", "Sport", "Politik", "Wirtschaft", "Panorama", "Wissenschaft", "Netzwelt", "Mobilität", "Reise",
              "Leben und Lernen", "Gesundheit"]

    #
    def add_arguments(self, parser):

        # if you set this param, stopwords will remain included, not setting the param will remove them from the texts
        parser.add_argument("-stops", action = "store_true")

        # you have the choice between creating new data (param set) or using the currently available data (param not set)
        parser.add_argument("-data", action = "store_true")

    def handle(self, *args, **options):

        if options["data"]:

            for i in self.topics:

                generate_nn_data(Article.objects.filter(publisher ='Spiegel'), i, options["stops"])

        start_sequential(options["stops"])