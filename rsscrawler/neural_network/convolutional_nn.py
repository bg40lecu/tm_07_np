import numpy as np
from keras.preprocessing.text import one_hot
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Activation, Dense, Dropout, Embedding, Conv1D, GlobalMaxPool1D
from sklearn.preprocessing import LabelBinarizer
import sklearn.datasets as skds
from pathlib import Path
import matplotlib.pyplot as plt
import itertools
from sklearn.metrics import confusion_matrix
import datetime

# Our try of the 1D convolutional neural network. Keras seems to think that the results are pretty good even though
# the confusion matrix tells us the result is pretty bad

def start_convolutional(stops):

    # uses a folder based on your stopword preferences
    if (stops):

        path = "analysis/data_with_stops"
    else:
        path = "analysis/data_without_stops"

    files = skds.load_files(path, load_content = False)

    # this vocab size seemed to be about right and resulted in the best accuracy
    vocab_size = 100000
    max_length = 0  # wird später errechnet
    epochs = 12
    batch_size = 16
    embedding_dims = 100
    filters = 250
    kernel_size = 3
    hidden_dims = 250

    label_index = files.target
    label_names = files.target_names
    labelled_files = files.filenames

    label_list = []

    # some preprocessing, mainly getting the texts from path, getting the related labels and
    # splitting training and test data

    for i in range(len(label_index)):
        label_list.append(label_names[label_index[i]])

    for file in labelled_files:

        if len(Path(file).read_text().split(" ")) > max_length:
            max_length = len(Path(file).read_text().split(" "))

    files_list = []

    # turns the text from string sequences into numeric sequences

    for file in labelled_files:
        files_list.append(one_hot(Path(file).read_text(), vocab_size))

    # pads the texts so every text has the same length (length of the longest text), if shorter the rest of the
    # vector is filled with 0's

    docs = pad_sequences(files_list, maxlen=max_length, padding="post")

    train_size = int(len(files_list) * 0.8)

    # turn the labels into one hot vectors

    encoder = LabelBinarizer()
    encoder.fit(label_list)

    labels = encoder.transform(label_list)

    train_docs = docs[:train_size]
    test_docs = docs[train_size:]

    train_labels = labels[:train_size]
    test_labels = labels[train_size:]

    model = Sequential()

    model.add(Embedding(vocab_size,
                        embedding_dims,
                        input_length = max_length))

    model.add(Dropout(0.25))

    model.add(Conv1D(filters,
                     kernel_size,
                     padding = 'valid',
                     activation = 'relu'))

    model.add(GlobalMaxPool1D())

    model.add(Dense(hidden_dims))

    model.add(Dropout(0.25))

    model.add(Activation('relu'))

    model.add(Dense(11))

    model.add(Activation('sigmoid'))

    model.summary()

    model.compile(loss = 'binary_crossentropy',
                  optimizer = 'adam',
                  metrics = ['accuracy'])

    history = model.fit(train_docs,
              train_labels,
              batch_size = batch_size,
              epochs = epochs,
              verbose = 1,
              validation_split=0.25)

    ##########################################

    # Plot training & validation accuracy values
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

    # Plot training & validation loss values
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.show()

    ##########################################

    # This is necessary to construct a confusion matrix which shows the percentages of the network classifying
    # an article in the wrong class

    fig = plt.figure()
    fig.set_size_inches(14, 12, forward=True)
    fig.align_labels()

    normalize = True
    title = 'Prognose des Netzes'
    cmap = plt.cm.Reds
    y_pred = model.predict(test_docs)
    cm = confusion_matrix(np.argmax(test_labels, axis=1), np.argmax(y_pred, axis=1))
    classes = np.asanyarray(label_names)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    if (normalize):

        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=90)
    plt.yticks(tick_marks, classes)

    fmt = ' .2f' if normalize else 'd'
    threshold = cm.max() / 2

    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > threshold else "black")

    date = datetime.datetime.now()

    fig.savefig('txt_classification-conv-' + str(date) + '.png', pad_inches=5.0)
    plt.show()
