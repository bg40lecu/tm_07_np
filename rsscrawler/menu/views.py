'''
Renders index.html
'''
from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse

# Create your views here.


def index_response(request):
    '''
    Renders index.html
    :return: The HTTP response with the index.html
    '''
    template = loader.get_template('index1.html')
    context = {}
    return HttpResponse(template.render(context, request))
