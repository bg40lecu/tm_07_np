import os
import spacy
from crawler.models import Scrape
from analysis.models import Words
import datetime
from django.db.utils import OperationalError as OperError
import operator


def binary_match_search(words_table_as_list, searched_string):
    searched_string = searched_string.lower()
    if len(words_table_as_list) > 1:
        pivot = words_table_as_list[round(len(words_table_as_list) / 2) - 1]
    elif len(words_table_as_list) == 0:
        raise Words.DoesNotExist
    else:
        pivot = words_table_as_list[0]
        if pivot.word_lemma.lower() != searched_string.lower():
            raise Words.DoesNotExist
    if pivot.word_lemma.lower() == searched_string.lower():
        return pivot
    elif pivot.word_lemma.lower() > searched_string.lower():
        return binary_match_search(words_table_as_list[:round(len(words_table_as_list) / 2)], searched_string)
    else:
        return binary_match_search(words_table_as_list[round(len(words_table_as_list) / 2):], searched_string)


def clean_articles(query_set):
    c = datetime.datetime.now()
    nlp = spacy.load('de', disable=['ner', 'parser'])

    article_dict_list = query_set
    article_arr = []

    # build an array of articles
    for article_dict in article_dict_list:
        cleaned_art = article_dict.get('article', 'none')

        # clean article by removing unnecessary characters
        cleaned_art = cleaned_art.replace(',', '').replace('\"', '').replace('\n', ' ').replace(':', ' ')
        #remove umlaute
        cleaned_art = cleaned_art.replace('ä', 'ae').replace('ö', 'oe').replace('ü', 'ue').replace('Ä', 'Ae').\
            replace('Ö', 'Oe').replace('Ü', 'Ue')
        cleaned_art = ' '.join(cleaned_art.split())

        # feeding cleaned article into article array to build docs from
        article_arr.append(cleaned_art)

    # build an array of docs
    article_docs = []
    for index, article in enumerate(article_arr):
        article_docs.append(nlp(article))
    b = datetime.datetime.now()
    #print(b - c)

    return article_docs


def update_words_by(queryset):
    final_dict = {}

    # clean articles and get doc list for given query
    cleaned_articles = clean_articles(queryset)

    for article in cleaned_articles:
        for word in article:
            if not(word.like_num or word.is_punct or word.like_url or word.like_email or word.is_upper or len(word.text) < 2 \
                    or word.pos_ == 'X' or word.text[0] == '/'):
                if word.lemma_ in final_dict.keys():
                    final_dict[word.lemma_][1] += 1
                else:
                    final_dict[word.lemma_] = [word.pos_, 1]
    fd_len = len(final_dict)
    #print(fd_len)
    w = list(Words.objects.all())
    count = 0
    start = datetime.datetime.now()
    for lemma, attributes in final_dict.items():
        # initialize or update words object
        try:
            binary_match_search(w, lemma)
            update = Words.objects.get(word_lemma=lemma)
            update.overall_quantity += attributes[1]
            update.save()
        except Words.DoesNotExist:
            Words(word_lemma=lemma, pos=final_dict[lemma][0], overall_quantity=final_dict[lemma][1]).save()
        except OperError:
            pass
        count += 1
        #print('\r{}% | time for lemma: {}'.format(round(100 * count / fd_len, 2), (datetime.datetime.now() - start)/count), end='')

# Creates folder and files for given Queryset and topic which will mainly be used to train a neural network later on
# takes into account if you want stopwords included or not
# (stops param set means you want the stopwords to be included, stops param not set removes them from the file

def generate_nn_data(Queryset, topic, stops):

    file = open("analysis/german_stopwords_plain_modified.txt", "r")
    stopwords = file.read().splitlines()

    topicpath = topic.lower()

    if(stops):

        directory = 'analysis/data_with_stops/' + topicpath

    else:

        directory = 'analysis/data_without_stops/' + topicpath

    if not os.path.exists(directory):

        os.makedirs(directory)

    spiegel_articles = Queryset

    spiegel_ids = []

    # getting the topic of an article
    # was written before data format of RSS entries was changed so remained hard coded

    for i in spiegel_articles:

        entry = str(i.entry)
        term_index = entry.find('\"term\"')
        scheme_index = entry.find('\"scheme\"')
        topic_of_i = entry[term_index + 9:(len(entry) - (len(entry) - (scheme_index - 3)))]

        if (topic_of_i == topic):
            spiegel_ids.append(i.id)

    # necessary because we don't want to use all of the topics to we have to filter some of them out

    spiegel_texts = Scrape.objects.filter(id__in = spiegel_ids)

    for text in spiegel_texts:

        words_in_article = []

        words = text.article.split()

        for word in words:

            # cleans the words

            word = word.replace("\"", "")
            word = word.replace(",", "")
            word = word.replace(".", "")
            word = word.replace("?", "")
            word = word.replace("!", "")
            word = word.replace("(", "")
            word = word.replace(")", "")
            word = word.replace(":", "")
            word = word.replace("]", "")
            word = word.replace("[", "")

            if (stops):

                words_in_article.append(word)

            else:

                for stopword in stopwords:

                    if (word != stopword and word != '' and word != '-'):

                        flag = True
                    else:
                        flag = False
                        break

                if (flag == True):
                    words_in_article.append(word)

        # write all the articles created accordingly to the params to a specified path

        with open(directory + "/" + text.id_id, "w+") as file:

            for i in words_in_article:
                file.write(i + " ")
