from django.db import models
from crawler.models import Article


class Keywords(models.Model):
    """
    Model to store the keywords for each article.
    The model is linked to the **Article**-model using a one-to-many relation
    """
    id = models.AutoField(primary_key=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    keyword = models.CharField(max_length=128)


class Words(models.Model):
    word_lemma = models.CharField(max_length=128, primary_key=True)
    pos = models.CharField(max_length=8)
    overall_quantity = models.IntegerField()
