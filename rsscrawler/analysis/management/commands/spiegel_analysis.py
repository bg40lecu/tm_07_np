from django.core.management.base import BaseCommand
from crawler.models import Article
from analysis.analysis_methods import generate_nn_data
from crawler.models import Scrape
import operator

class Command(BaseCommand):

    topics = ["Kultur", "Sport", "Politik", "Wirtschaft", "Panorama", "Wissenschaft", "Netzwelt", "Mobilität", "Reise", "Leben und Lernen", "Gesundheit"]

    def add_arguments(self, parser):
        parser.add_argument("-stops", action="store_true")


    def handle(self, *args, **options):

        # print(spiegel_terms(Article.objects.filter(publisher = 'Spiegel')))

        # print(words_by_topic_spiegel(Article.objects.filter(publisher = 'Spiegel'), 'Politik'))

        for i in self.topics:

            generate_nn_data(Article.objects.filter(publisher ='Spiegel'), i, options["stops"])





