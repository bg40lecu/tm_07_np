"""
When used as a django-command, it plots a cloud of points.
Points resemble articles. When points are close together, the
represented articles have similar keywords. Points can be
painted by their true topic or by the best topic genrated
by a topic-modeling approach. Points can also be labeld by
their true topic or by a random keyword which the article
contains.
"""
from django.core.management.base import BaseCommand
from crawler.models import Article
from crawler.models import Scrape
from analysis.models import Keywords
import nltk
from nltk.stem.snowball import SnowballStemmer
from nltk.tokenize import word_tokenize
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_distances
from sklearn.manifold import MDS
import gensim
from collections import defaultdict
from random import random
from random import choice
from random import sample
from json import loads


class Command(BaseCommand):
    """
    Django-command to plot a cloud of points.
    Points resemble articles. When points are close together, the
    represented articles have similar keywords. Points can be
    painted by their true topic or by the best topic genrated
    by a topic-modeling approach. Points can also be labeld by
    their true topic or by a random keyword which the article
    contains.
    """
    
    def handle(self, *args, **options):
        """
        First method to be evoked when used as a django-command.
        A cloud of points is ploted.
        Firts random articles, their keywords and real topic are loaded.
        From a topic modeling, topics are predicted for each article.
        Then the documentTermMatrix for the keywords of the articles is caculated
        and from that the distance-matrix using the cosine-distance, which can 
        refined with the logarithm to force clustering. 
        with multidimensional-scaling the coordinates for the points are calculated
        using the distance-matrix. Plots can be specified by editing the clt variable.
        There are multiple variables which can be modified to detemine the outcome:
            * **plotdimensions**: 2 for circle, 3 for sphere
            * **numtopics**: number of topics generated by the topic-modeling
            * **articleNumber**: number of articles ploted
            * **labes1outof**: 1 out of `number` articles will be labeld
            * **save**: True if plots should be saved as png
            * **publisher**: publisher to generate point-cloud for
            * **clt**: list of 3-tuple which each specifie a plot
                * first arg: list of colors/numbers for each point/articel
                * second arg: list of labels for each point/articel
                * third arg: title of the plot
        """
        plotdimensions = 2
        numtopics = 10
        articleNumber = 1000 # None for all
        labels1outof = 10
        save = True
        publisher = "BILD"

        print("Query")
        artids, keywords, scrapes = self.queryKeywords(publisher=publisher, artNumber=articleNumber)

        print("Topic Modeling")
        topicsmodel = self.topicModeling(scrapes, publisher, numtopics)
        print("Topic Querying")
        topicsreal = self.queryTopics(artids)

        distincttopics = list(set(topicsreal))
        topicmap = dict(zip(distincttopics, range(len(distincttopics))))
        topicsrealnum = [topicmap[topic] for topic in topicsreal]
        
        corpus = [",".join(keys) for keys in keywords]

        print("DocumentTermMatrix")
        dtm = self.documentTermMatrix(corpus)

        print("Distances")
        dist = cosine_distances(dtm)
        #dist = -np.log((-dist+1.1)/1.1)
        
        print("Multidimensional Scaling")
        positions = self.simPositions(dist, dims=plotdimensions, runs=4, iterations=100)
        positions = zip(*positions)

        labelskeywords = [choice(keyword) if choice(range(labels1outof)) == 0 else "" for keyword in keywords]
        labelstopics = [topic if choice(range(labels1outof)) == 0 else "" for topic in topicsreal]
        colorsreal =[topic for topic in topicsrealnum]
        colorsmodel =[topic for topic in topicsmodel]

        clt = [
            (None, None, publisher + " (" + str(articleNumber) + " - uncolored)"),
            (colorsmodel, labelskeywords, publisher + " (" + str(articleNumber) + " - topicmodeling " + str(numtopics) + " - keywords)"),
            (colorsreal, labelstopics, publisher + " (" + str(articleNumber) + " - real topic - topics)"),
        ]

        self.plot(*positions, colorslabelstitle=clt, cmap=cm.nipy_spectral, save=save)


    def topicModeling(self, corpus, publisher, numtopics=15):
        """
        Performs a topic-modeling on all articles of a given publisher and returns
        a list of the best topic for the given articles.

        :param corpus: list of articles for which the topic should be specified
        :param publisher: publisher for whom the articles should be used to performe topic-modeling
        :param numtopics: number of topics for the topic-modeling
        :returns: list of best topics for the given corpus, topics for the articles are in the same order 
                  like the articles in the corpus
        """
        scrapes = map(lambda x: x["article"], Scrape.objects.filter(id__publisher = publisher).values("article").all())

        def clean(corpus):
            texts = [gensim.utils.simple_preprocess(doc, deacc=True) for doc in corpus]
            stopwords = nltk.corpus.stopwords.words("german")
            stopwords.extend(["dass"])
            texts = [[e for e in bow if e.lower() not in stopwords] for bow in texts]
            stemmer = SnowballStemmer("german")
            texts = [[stemmer.stem(e) for e in bow] for bow in texts]

            #textstmp = []
            #for text in texts:
            #    wordcount = defaultdict(int)
            #    for word in text:
            #        wordcount[word] += 1
            #    newtext = []
            #    for word, freq in wordcount.items():
            #        if freq > 1:
            #            newtext.extend([word]*freq)
            #    textstmp.append(newtext)
            #texts = textstmp
            return texts
            
        corpus = clean(corpus)
        traincorpus = clean(scrapes)
        dictionary = gensim.corpora.Dictionary(traincorpus)
        corpus = [dictionary.doc2bow(text) for text in corpus]
        traincorpus = [dictionary.doc2bow(text) for text in traincorpus]
        ldamodel = gensim.models.LdaModel(traincorpus, num_topics = numtopics, id2word=dictionary)
        
        def best(topics):
            besttopic = None
            lastvalue = 0
            for topic, value in topics:
                if value > lastvalue:
                    besttopic = topic
                    lastvalue = value
            return besttopic
        
        return [best(topics) for topics in ldamodel[corpus]]

    
    def queryTopics(self, artids):
        """
        Loads all topics for the articles from the database, for which the ids are specified in artids.
        
        :param artids: ids of the articles, for which the topic should be loaded
        :returns: topics of the articles in the same order as the ids are in artids
        """
        entrys = []
        for artid in artids:
            article = Article.objects.filter(id = artid).values("entry")
            entrys.append(loads(article[0]["entry"]))
        
        topics = []
        for entry in entrys:
            try:
                topics.append(entry["tags"][0]["term"])
            except Exception: topics.append("")

        return topics
        

    def queryKeywords(self, publisher, artNumber=None):
        """
        Loads a cerain amount of random articles, which have at least one keyword each, for a publisher.

        :param publisher: publisher for which articles should be loaded
        :param artNumber: number of articles that should be loaded
        :returns: 3-tuple of list of article-ids, list of keywords for each article and list of the actual articles
        """
        if artNumber:
            artids = set(map(lambda x: x["article_id"], Keywords.objects.filter(article_id__publisher=publisher).values("article_id")))
            try:
                artids = sample(list(artids), artNumber)
            except Exception:
                pass
            articleKeywords = list(Keywords.objects.filter(article_id__in = artids).values("keyword", "article_id").all())
        else:
            articleKeywords = list(Keywords.objects.filter(article_id__publisher = publisher).values("keyword", "article_id").all())

        keywordsdict = defaultdict(list)
        for articleKeyword in articleKeywords:
            artid = articleKeyword["article_id"]
            keyword = articleKeyword["keyword"]
            keywordsdict[artid].append(keyword)

        artids = []
        keywords = []
        scrapes = []
        for artid, keys in keywordsdict.items():
            artids.append(artid)
            keywords.append(keys)
            scrape = Scrape.objects.filter(id = artid).values("article").all()[0]["article"]
            scrapes.append(scrape)

        return artids, keywords, scrapes
        

    def documentTermMatrix(self, corpus):
        """
        Computes a sparse document-term-matrix for the given corpus.

        :param copus: corpus for which the dtm should be computed
        :returns: sparse document-term-matrix for the given corpus
        """
        vectorizer = CountVectorizer(tokenizer=lambda s: s.split(","), lowercase=False)
        return vectorizer.fit_transform(corpus)
        

    def simPositions(self, dist, dims=2, runs=4, iterations=300):
        """
        Computes the point-cloud for the given distance-matrix via multidimensional-scaling.
        
        :param dist: distance-matrix for which the point-cloud should be computed
        :param dims: dimensions in which the point-cloud expands
        :param runs: number of point-clouds that should be computed (best is returned)
        :param iterations: number of interations for computing the point-cloud
        :returns: list of points in the given dimension (dims dimensional tuples)
        """
        mds = MDS(n_components=dims, dissimilarity = "precomputed", random_state=1, n_jobs=-1, n_init=runs, max_iter=iterations, verbose=2)
        return mds.fit_transform(dist)


    def plot(self, xpos, ypos, zpos=None, colorslabelstitle=[], cmap=None, save=False):
        """
        Plots points with the specified coordinates. When zpos is specified, the points are
        ploted in the 3rd-dimension. Multiple color- and labelstyles can be specified by the
        varibale colorslabeltitle

        :param xpos: x-coordinate of the points
        :param ypos: y-coordinate of the points
        :param ypos: z-coordinate of the points, None for 2nd-dimension plot
        :param colorslabelstitle: list of 3-tuples specifying the colors, lables and title
                                  for a plot, for each tuple one plot is generated
        :param cmap: colormap which maps the colors given as numbers to actual colors
        :param save: True if the plots should be saved as png, title is choosen as the name
        """
        if zpos:
            for colors, labels, title in colorslabelstitle:
                fig = plt.figure()
                ax = fig.add_subplot(111, projection='3d')
                ax.set_axis_off()
                ax.scatter(xpos, ypos, zpos, c=colors, cmap=cmap)
                if labels:
                    for x, y, z, label in zip(xpos, ypos, zpos, labels):
                        ax.text(x, y, z, label)
                plt.title(title)
                if save:
                    plt.savefig(title)
                    
        else:
            for colors, labels, title in colorslabelstitle:
                plt.figure()
                plt.scatter(xpos, ypos, c=colors, cmap=cmap)
                if labels:
                    for x, y, label in zip(xpos, ypos, labels):
                        plt.text(x, y, label, fontsize=6)
                plt.title(title)
                cur_axes = plt.axes()
                cur_axes.get_xaxis().set_visible(False)
                cur_axes.get_yaxis().set_visible(False)
                if save:
                    plt.savefig(title)

        plt.show()
        
