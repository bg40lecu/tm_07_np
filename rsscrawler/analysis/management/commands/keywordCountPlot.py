"""
When used as a django-command, it plots the number of used keywords, distinct keywords, articlenumber,
for each publisher and number of used keywords and distinct keywords per article for each publisher.
"""

from django.core.management.base import BaseCommand
from crawler.models import Article
from analysis.models import Keywords
import matplotlib.pyplot as plt
import numpy as np

class Command(BaseCommand):
    """
    Django-command to plot the number of used keywords, distinct keywords and articlenumber
    for each publisher and number of used keywords and distinct keywords per article for each publisher.
    """
    
    def handle(self, *args, **options):
        """
        First method to be evoked when used as a django-command.
        It retrieves all keywords and the number of articels for each publisher
        and cacluclates the number of keywords and disinct keywords, as well as the
        number of used keywords and distinct keywords per article for each publisher.
        It then plots the number of used keywords, distinct keywords and articlenumber
        for each publisher and number of used keywords and distinct keywords per article for each publisher.
        """
        publishers = set(map(lambda x: x["publisher"], Article.objects.values("publisher")))
        numKeyList = []
        avgNumKeyList = []

        for publisher in publishers:
            keywords = Keywords.objects.filter(article__publisher = publisher).all()
            numArts = Article.objects.filter(publisher = publisher).all().count()
            keywords = list(map(lambda x: x.keyword, keywords))
            numKeys = len(keywords)
            numDistinctKeys = len(set(keywords))
            numKeyList.append((publisher, numKeys, numDistinctKeys, numArts))
            avgNumKeyList.append((publisher, numKeys/numArts, numDistinctKeys/numArts))

        publishers, numKeys, numDestinctKeys, numArts = list(zip(*sorted(numKeyList, key=lambda x: x[1], reverse=True)))
        
        N = len(publishers)
        ind = np.arange(N)
        width = 0.25

        plt.subplot(2, 1, 1)
        plt.bar(ind - width, numKeys, width, label="Alle Keywords")
        plt.bar(ind, numDestinctKeys, width, label="Eindeutige Keywords")
        plt.bar(ind + width, numArts, width, label="Artikelanzahl")

        plt.title('Keywordvielfalt')
        plt.xticks(ind, publishers)
        plt.legend()

        publishers, avgNumKeys, avgDestinctKeys = list(zip(*sorted(avgNumKeyList, key=lambda x: x[1], reverse=True)))
        
        plt.subplot(2, 1, 2)
        plt.bar(ind - width, avgNumKeys, width, label="Keywords pro Artikel")
        plt.bar(ind, avgDestinctKeys, width, label="Neue Keywords pro Artikel")

        plt.title('Keywords pro Artikel')
        plt.xticks(ind, publishers)
        plt.legend()
        plt.tight_layout(pad=0.5)
        plt.savefig("Keywordverwendung")
        plt.show()
