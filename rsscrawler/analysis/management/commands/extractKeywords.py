"""
Provides classes for extraxting keywords from the crawled Articles.

When used as a django-command, it reads all Articles from the 
Database using the :class:`Article`-model and extracts the keywords from the html meta-tag **news_keywords**,
which is itself contained in the html-head. Afterwards the keywords are 
written to the database using the :class:`Keywords`-model.
The data is read in chunks of 100 Articles. This allows to process and read the
data in parallel. The number of processes to extract the keywords is defined by
the global variable **processnumber**.
Keywords of an article are only extracted if there is no entry for that article
in the keyword-table.
"""

from django.core.management.base import BaseCommand
from crawler.models import Article
from analysis.models import Keywords
from bs4 import BeautifulSoup
from re import split
from threading import Thread
from multiprocessing import Process, Queue
from queue import Empty
from json import dumps
from math import floor

processnumber = 3 # number of processes for extraction

class ArticleFetcher(Thread):
    """
    Queries the Database to load all articles with the given ids concurrently.
    Loaded articles are stored in the membervarible **arts**.
    """
    def __init__(self, ids):
        """
        Initialises the ids of the articles to load.

        :param ids: list of article-ids
        """
        Thread.__init__(self)
        self.ids = ids
    
    def run(self):
        """
        Overwrites **Thread.run**.
        When evoked by **Thread.start**, it loads the articles from the Database
        and stores them in the membervarible **arts**.
        """
        self.arts = list(Article.objects.filter(id__in = self.ids).all().iterator())


class KeywordExtractor(Process):
    """
    Extracts keywords from a given number of documents in a separate process
    and writes the constructed **Keywords**-objects to a **multiprocessing.Queue**.
    """
    def __init__(self, arts, queue):
        """
        Initialises the articles to extract keywords from and the queue to write the data to.

        :param arts: iteratable containing **Article**-objects
        :param queue: **multiprocessing.Queue** to write data to
        """
        Process.__init__(self)
        self.arts = arts
        self.queue = queue

    def run(self):
        """
        Overwrites **Process.run**.
        When evoked by **Process.start**, it extracts all keywords
        from the given articles and writes constructed **Keywords**-objects to the queue.
        """
        for art in self.arts:
            soup = BeautifulSoup(art.html, 'lxml')

            head = soup.head
            if head:
                meta = head.meta
                while meta:
                    if meta.get("name") == "news_keywords":
                        content = meta.get("content")
                        if content:
                            for keyword in split("\s*,\s*", content):
                                self.queue.put(Keywords(article=art, keyword=keyword))
                            break
                    meta = meta.find_next("meta")


class QueueExtractor(Thread):
    """
    Reads data from a **multiprocessing.Queue**-object concurrently
    and writes the data to a list.
    """

    def __init__(self, queue):
        """
        Initialises the queue to read from and the internal list.

        :param queue: **multiprocessing.Queue** to read from
        """
        Thread.__init__(self)
        self.elements = []
        self.queue = queue
    
    def pop(self):
        """
        Returns all elements read so far and resets the list to write to.

        :returns: list of all elements read so far
        """
        els = self.elements
        self.elements = []
        return els

    def run(self):
        """
        Overwrites **Thread.run**.
        When evoked by **Thread.start**, it reads elements from queue and writes them to 
        the internal list until **QueueExtractor.stop** is evoked.
        """
        self.running = True
        while self.running:
            try:
                self.elements.append(self.queue.get_nowait())
            except Empty:
                pass

    def stop(self):
        """
        Stops the thread.
        """
        self.running = False
        

def splitList(l, parts):
    """
    Tries to split a list in equaly sized parts.

    :param l: list to split
    :param parts: number of parts

    :returns: list of equaly sized lists, containing the input elements
    """
    length = len(l)
    partslength = length/parts
    return [l[floor(start*partslength):floor((start+1)*partslength)] for start in range(parts-1)] + [l[floor((parts-1)*partslength):length]]


class Command(BaseCommand):
    """
    Django-command to read all articles from the database,
    extract the keywords and write the extracted keywords to the database.
    """
    
    def handle(self, *args, **options):
        """
        First method to be evoked when used as a django-command.
        It read all Articles from the Database,
        extract the keywords and write the extracted Keywords to the database.
        The data is read in chunks of 100 Articles. This allows to process and read the
        data in parallel. The number of processes to extract the keywords is defined by
        the global variable **processnumber**.
        Keywords of an article are only extracted if there is no entry for that article
        in the keyword-table.
        Progess is printed to the console.
        """
        already_there = Keywords.objects.values('article')
        new_entry_ids = Article.objects.values('id').exclude(id__in=already_there).all()
        new_entry_ids = list(new_entry_ids)

        print(str(len(new_entry_ids)) + " articles with extractable keywords")
        
        idsplit = []
        splitsize = 100
        while new_entry_ids:
            idsplit.append(map(lambda x: x["id"], new_entry_ids[:splitsize]))
            new_entry_ids = new_entry_ids[splitsize:]
            
        art_fetcher = ArticleFetcher(idsplit.pop())
        art_fetcher.start()
        art_fetcher.join()
        arts = art_fetcher.arts

        queue = Queue()
        qextr = QueueExtractor(queue)
        qextr.start()

        while idsplit:
            ids = idsplit.pop()
            print("\r{} still to process".format(len(idsplit), end=""))

            parts = splitList(arts, processnumber)

            threads = []
            for part in parts:
                process = KeywordExtractor(part, queue)
                process.start()
                threads.append(process)

            art_fetcher = ArticleFetcher(ids)
            art_fetcher.start()

            for thread in threads:
                thread.join()

            art_fetcher.join()
            arts = art_fetcher.arts

            new_keywords = qextr.pop()

            Keywords.objects.bulk_create(new_keywords)
        
        qextr.stop()

        new_keywords = qextr.pop()

        Keywords.objects.bulk_create(new_keywords)

