"""
When used as a django-command, it plots the most used keywords with
their frequency for each publisher as a pie-plot.
"""

from django.core.management.base import BaseCommand
from crawler.models import Article
from analysis.models import Keywords
import matplotlib.pyplot as plt
import numpy as np
from collections import defaultdict
from math import ceil

class Command(BaseCommand):
    """
    Django-command to plot the most used keywords with
    their frequency for each publisher as a pie-plot.
    """
    
    def handle(self, *args, **options):
        """
        First method to be evoked when used as a django-command.
        It queries the database to count each keyword for each publisher
        and plots the most used keywords with their frequency
        for each publisher as a pie-plot. The plot is saved as a graphic.
        """
        publishers = set(map(lambda x: x["publisher"], Article.objects.values("publisher")))
        numKeywords = 10
        publKeyCount = []

        for publisher in publishers:
            keywords = Keywords.objects.filter(article__publisher = publisher).all()
            keywords = list(map(lambda x: x.keyword, keywords))
            keyCount = defaultdict(int)
            for keyword in keywords:
                keyCount[keyword] += 1
            publKeyCount.append((publisher, keyCount))
        
        keywordRanking = dict()
        for publisher, keyCount in publKeyCount:
            listKeyCount = list(keyCount.items())
            listKeyCount = sorted(listKeyCount, key=lambda x: x[1], reverse=True)
            maxlistKeyCount = listKeyCount[:numKeywords]
            maxlistKeyCount.reverse()
            keywordRanking[publisher] = maxlistKeyCount

        N = numKeywords
        ind = np.arange(N)
        width = 0.8

        index = 1
        for publisher, keyCount in keywordRanking.items():
            
            keywords, counts = zip(*keyCount)
            plt.subplot(ceil(len(keywordRanking)/2), 2, index)
                
            plt.barh(ind, counts, width)

            plt.title(publisher)
            plt.yticks(ind, keywords)

            index += 1

        plt.subplots_adjust(wspace=0.7, right=0.9, left=0.2, hspace=0.4)
        plt.savefig("KeywordsBarPlot")
        plt.show()
