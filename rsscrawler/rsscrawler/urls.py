"""djangoproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from logger import views as logger_views
from visuals import views as visuals_views
from menu import views as menu_views

urlpatterns = [
    path('logs/latest', logger_views.logging_web,),
    path('logs/by_date', logger_views.logs_by_date),
    path('logs/few_duplicates', logger_views.last_0),
    path('admin/', admin.site.urls),
    path('visuals/logs', visuals_views.render_log),
    path('visuals/article_q', visuals_views.render_article_quant),
    path('visuals/article_perc', visuals_views.render_article_perc),
    path('visuals/char_word_count', visuals_views.render_word_char_length),
    path('visuals/publish_heatmap', visuals_views.show_publish_heatmap),
    path('visuals/length_heatmap', visuals_views.show_length_heatmap),
    path('', menu_views.index_response),
]
