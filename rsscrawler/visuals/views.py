from django.shortcuts import render
from chartit import PivotChart, PivotDataPool, DataPool, Chart
from django.template import loader
from crawler.models import Article, Log, Scrape, ArticleTime
from django.http import HttpResponse
from datetime import datetime
from django.db.models import Max, Count, ExpressionWrapper, DecimalField, F, Func, Value
from django.db.models.functions import Length, ExtractWeekDay, ExtractHour
from django.db.models.aggregates import Sum
from visuals.heatmap import heatmap, annotate_heatmap
import matplotlib.pyplot as plt
import numpy as np

import time


def create_publisher_chart():
    #Step 1: Create a DataPool with the data we want to retrieve.
    articledata = \
        DataPool(
        series=[{
            'options': {
                'source':
                    Article.objects.values('publisher').annotate(Count('id'))
            },
            'terms': [
                'publisher',
                'id__count',

            ]
        }]
        )

    #Step 2: Create the Chart object
    cht = Chart(
        datasource = articledata,
        series_options =[{
            'options':{
                'type': 'column',
                'stacking': False
            },
            'terms': {
                'publisher': ['id__count']
            }
        }],
        chart_options ={
            'title': {
                'text': 'Article Quantities per Publisher'
            },
            'subtitle': {
                'text': 'Total {} articles'.format(Article.objects.count())
            },
            'xAxis': {
                'title': {
                    'text': 'Publisher'
                }
            },
            'chart':
                {
                    'height': str(9 / 16 * 100) + '%',
                },
        }
    )
        #Step 3: Send the chart object to the template.

    return cht

def create_log_chart():
    logdata = \
        DataPool(
            series=
                [{'options': {
                    'source': Log.objects.values('timestamp', 'new_entries').order_by('timestamp').reverse()},
                    'terms': [
                        ('timestamp', lambda d: time.mktime(d.timetuple())),
                        'new_entries']
                }]
        )
    cht = Chart(
        datasource=logdata,
        series_options=
        [{'options': {
            'type': '',
            'stacking': False},
            'terms': {
                'timestamp': [
                    'new_entries']
                }
        }],
        chart_options=
        {
            'chart':
                {
                    'height': str(9/16 * 100) + '%',
                },
            'title':
            {
                'text': 'New entries per crawl'
            },
            'xAxis': {
                'title': {
                    'text': 'Time'
                },
                'min': Log.objects.count() - 672,
            },
            'yAxis': {
                'min': 0,
                'max': 230,
            }

        },


        x_sortf_mapf_mts=(None, lambda i: datetime.fromtimestamp(i).strftime("%a %d.%m.%m %H:%M"), False))
    
    return cht


def create_pie_chart_articles():
    art_data = \
        DataPool(
                series=[{'options': {
                    # For some reason max_digits has no effect, still shows 4 decimal places
                    'source': Article.objects.values('publisher').annotate(pct=
                            ExpressionWrapper((100*Count('id') / Article.objects.count()), output_field=
                                                                    DecimalField(max_digits=4, decimal_places=2)
                            )
                        )


                    },
                    'terms': [
                        {'Publisher': 'publisher'},
                        {'Percentage of total feeds': 'pct'}
                    ]

                }]
        )
    cht = Chart(
        datasource=art_data,
        series_options=
        [
            {
                'options': {
                    'type': 'pie',
                    'stacking': False,
                },
                'terms': {
                    'Publisher': [
                        'Percentage of total feeds'
                    ],
                }
            }
        ],
        chart_options=
        {
            'chart':
                {
                    'height': str(9 / 16 * 100) + '%',
                },
            'title':
                {
                    'text': 'Article Percentages',
                },
            'subtitle':
                {
                    'text': 'Total {} articles'.format(Article.objects.count())
                },
        }
    )

    return cht


def character_count_chart():
    data = \
        DataPool(
            series=[
                {'options':
                    {
                        'source': Scrape.objects.all().annotate(char_count=
                                    (Length('article') -
                                        (2 * Length('article') -
                                            Length(
                                                Func(
                                                    F('article'), Value('\n'), Value(''), function='replace'
                                                )
                                            ) -
                                            Length(
                                                Func(
                                                    F('article'), Value(' '), Value(''), function='replace'
                                                )
                                            )
                                            )
                                    )
                                                       ).filter(char_count__gt=0).values('id__publisher').annotate(
                                        avg_cc_per_pub=(Sum('char_count')/Count('id__publisher'))
                                    )
                    },
                    'terms': [
                        {
                            'Publisher': 'id__publisher',
                            'Average character count per article': 'avg_cc_per_pub'
                        }
                    ]
                },
                {'options': {
                    'source': Scrape.objects.all()
                            .annotate(word_count=(
                                2 * Length('article') -
                                Length(
                                    Func(
                                        F('article'), Value('\n'), Value(''), function='replace'
                                    )
                                ) -
                                Length(
                                    Func(
                                        F('article'), Value(' '), Value(''), function='replace'
                                    )
                                )
                                + 1
                            )
                        )
                        .filter(word_count__gt=0).values('id__publisher').annotate(
                        avg_word_count=(Sum('word_count') / Count('id__publisher'))
                    )

                },
                    'terms': [
                        {'Publisher_w': 'id__publisher',
                         'Average word count per article': 'avg_word_count',
                        }
                    ]
                }
            ]
        )
    cht = Chart(
        datasource=data,
        series_options=
        [
            {
                'options': {
                    'type': 'bar',
                    'stacking': False,
                },
                'terms': {
                    'Publisher': [
                        'Average character count per article',

                    ],
                    'Publisher_w': [
                        'Average word count per article',
                    ]
                }
            }
        ],
        chart_options=
        {
            'title':
                {
                    'text': 'Article Character/Word Count Average per Publisher',
               },
            'chart':
                {
                    'height': str(9 / 16 * 100) + '%',
                },
        }
    )

    return cht



# def published_heatmap():
#
#     data = \
#         DataPool(
#             series=[
#                 {'options':
#                      {
#                         'source': ArticleTime.objects.values('dow', 'hour').annotate(
#                             avg_val=ExpressionWrapper(Count('timestamp')/Count('timestamp', distinct=True),
#                                                       output_field=DecimalField())
#                         )
#                      },
#
#                 'terms': [{
#                     'Day of week': 'dow',
#                     'Hour': 'hour',
#                     'Entries that hour': 'avg_val',
#                 }]
#                  }
#             ]
#         )
#     cht = Chart(
#         datasource=data,
#         series_options=[
#             {
#                 'options': {
#                     'type': 'heatmap',
#                 },
#                 'terms': {
#                     'Hour': ['Day of week'],
#                 }
#             }
#         ],
#         chart_options=
#         {
#             'title':
#                 {
#                     'text': 'Article Heatmap',
#                 },
#             'chart':
#                 {
#                     'height': str(9 / 16 * 100) + '%',
#                 },
#             'xAxis':
#                 {
#                     'categories': range(0, 24),
#                 },
#             'yAxis':
#                 {
#                     'categories': range(0, 7),
#                 },
#             'series': [{
#                 'name': 'Publishes per hour and weekday',
#                 'borderWidth': 1,
#                 'data': data,
#             }]
#         }
#     )
#
#     return cht


def render_log(request):

    chart = create_log_chart()
    template = loader.get_template('chart.html')
    return HttpResponse(template.render({'charts': [chart]}, request))


def render_article_quant(request):

    chart = create_publisher_chart()
    template = loader.get_template('chart.html')
    return HttpResponse(template.render({'charts': [chart]}, request))


def render_article_perc(request):

    chart = create_pie_chart_articles()
    template = loader.get_template('chart.html')
    return HttpResponse(template.render({'charts': [chart]}, request))


def render_word_char_length(request):

    chart = character_count_chart()
    template = loader.get_template('chart.html')
    return HttpResponse(template.render({'charts': [chart]}, request))


def show_publish_heatmap(request):
    print(len(request.GET))
    if len(request.GET) > 0:
        thema = request.GET['thema']

        if thema == 'all':
            averages_list = list(ArticleTime.objects.values('dow', 'hour').annotate(cnt=Count('timestamp')).annotate(
                                   cnt_distinct=Count('timestamp', distinct=True)
                            ))
        else:
            averages_list = list(ArticleTime.objects.filter(id__thema=thema).values('dow', 'hour').annotate(cnt=Count('timestamp')).annotate(
                cnt_distinct=Count('timestamp', distinct=True)
            ))
        averages = []
        for i in range(0, 7):
            averages_day = []
            for j in range(0, 24):
                for dic in averages_list:
                    if dic['dow'] == i and dic['hour'] == j:
                        averages_day.append(dic['cnt']/dic['cnt_distinct'])
            averages.append(averages_day)

        averages_np = np.array(averages)

        fig, ax = plt.subplots()
        ax.set_title('Publishes per day and hour for category: ' + thema)
        # fig.suptitle('Publishes per day and hour for category: ' + thema)
        im = heatmap(averages_np, ax=ax, cmap="Oranges")
        # cbar = heatmap(averages_np, ax=ax, cmap='Oranges', cbarlabel='publishes')

        texts = annotate_heatmap(im)


        fig.tight_layout(rect=[0, 0, 1, 1.5])
        plt.savefig('visuals/static/plots/publish_heatmap_{}.png'.format(thema), dpi=250)
        plt.close(fig)

    template = loader.get_template('matplots.html')
    context = {'plotname': 'publish_heatmap_{}'.format(request.GET['thema'])}
    return HttpResponse(template.render(context, request))


def show_length_heatmap(request):
    print(len(request.GET))
    if len(request.GET) > 0:
        publisher = request.GET['publisher']

        if publisher == 'all':
            averages_list = list(ArticleTime.objects.values('dow', 'hour').annotate(avg_len=(Sum('art_length')/Count('timestamp'))))
        else:
            averages_list = list(ArticleTime.objects.filter(id__publisher=publisher).values
                                 ('dow', 'hour').annotate(avg_len=(Sum('art_length')/Count('timestamp'))))

        averages = []
        for i in range(0, 7):
            averages.append([0] * 24)
        for i in range(0, 7):
            averages_day = []
            for j in range(0, 24):
                articles_found = False
                for dic in averages_list:
                    if dic['dow'] == i and dic['hour'] == j:
                        averages[i][j] = dic['avg_len']

        averages_np = np.array(averages)

        fig, ax = plt.subplots()
        ax.set_title('Average article length per day and hour for publisher: ' + publisher)
        # fig.suptitle('Publishes per day and hour for category: ' + thema)
        im = heatmap(averages_np, ax=ax, cmap="Oranges")
        # cbar = heatmap(averages_np, ax=ax, cmap='Oranges', cbarlabel='publishes')

        texts = annotate_heatmap(im, valfmt='{x:d}')

        fig.tight_layout(rect=[0, 0, 1, 1.5])
        plt.savefig('visuals/static/plots/length_heatmap_{}.png'.format(publisher), dpi=250)
        plt.close(fig)

    template = loader.get_template('matplots.html')
    context = {'plotname': 'length_heatmap_{}'.format(request.GET['publisher'])}
    return HttpResponse(template.render(context, request))


# def render_article_heatmap(request):
#
#     chart = published_heatmap()
#     template = loader.get_template('chart.html')
#     return HttpResponse(template.render({'charts': [chart]}, request))
