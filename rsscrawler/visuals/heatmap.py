import numpy as np
import matplotlib.pyplot as plt
import matplotlib


def heatmap(data, ax=None, cbar_kw={}, cbarlabel="", **kwargs):

    font = {'family': 'normal',
            'size': 4.5}

    matplotlib.rc('font', **font)

    if not ax:
        ax = plt.gca()

    # Plot heatmap
    im = ax.imshow(data, **kwargs)

    # Create sidebar (colorbar)
    #cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    #cbar.ax.set_ylabel(cbarlabel, rotation=-90, va='bottom')

    # tick setting
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # labels for ticks
    ax.set_yticklabels(['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'])
    ax.set_xticklabels(list(range(0, 24)))

    plt.xticks(fontsize=7)
    plt.yticks(fontsize=7)

    return im  # , cbar


def annotate_heatmap(im, data=None, valfmt='{x:.2f}', textcolors=['black', 'white'],
                     threshold=None, **textkw):


    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max()) / 2

    # set standard alignment but overwritable by textkw
    kw = dict(horizontalalignment='center', verticalalignment='center')
    kw.update(textkw)

    # get value formatter
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # paste data to image
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[im.norm(data[i, j]) > threshold])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts
